# ADE environment for Project Aslan

This project sets up a base image to be used with the [ADE CLI](https://ade-cli.readthedocs.io/en/latest/index.html) tool.

## Usage

### Install Docker

Make sure you have the latest version of Docker installed by following the official installation instructions:

* [Ubuntu](https://docs.docker.com/engine/install/ubuntu/)

To be able to use NVIDIA GPUs, also install the [NVIDIA Docker Toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker).
When on a version of Ubuntu newer than 20.04, the step to set up the `stable` repository will fail.
You can use the repository for 20.04 by running the following instead:
```
distribution=ubuntu20.04 \
   && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - \
   && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
```

### Install ADE

Follow the [documentation of ADE](https://ade-cli.readthedocs.io/en/latest/index.html) to install the `ade` command line tool.

### Build Docker image

Clone this repository, and build the Docker image:
```
cd ade-aslan
docker build -t ade-aslan .
```
If this fails due to permissions, do *not* run with `sudo`.
Instead, go back to the Docker installation instructions and perform the step to add yoruself to the `docker` group which you have missed.

### Create a home

When running ADE it will be like logging into a different machine with a separate home directory.
This directory is actually a directory on your machine that gets mounted inside of the container, so you can easily share files in and outside of the environment.
You need to create this folder and mark it as the home folder for ADE:
```
mkdir adehome
cd adehome
touch .adehome
```

Next you need to specify what images and volumes ADE should use when starting it inside of this home directory.
Still inside of `adehome`, create a file called `.aderc` with the following content:
```
# Uncomment the following line to enable access to your camera
# export ADE_DOCKER_RUN_ARGS="--device /dev/video0"
export ADE_GITLAB=gitlab.com
export ADE_REGISTRY=registry.gitlab.com
export ADE_IMAGES="
  ade-aslan:latest
#  registry.gitlab.com/apexai/ade-vscode:latest
#  registry.gitlab.com/sgvandijk/ade-emacs:18.04-27.2
"
```

Uncommenting one of the last lines brings in the respective editor into the environment.
There is also [Atom](https://gitlab.com/ApexAI/ade-atom) for who likes that.

### Start the environment

Now you should be ready to start and enter the environment with:
```
ade start
ade enter
```
