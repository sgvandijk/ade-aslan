FROM ros:melodic

RUN apt-get update && \
    apt-get install -y \
    gnome-terminal \
    openssh-client \
    wget \
    python-psutil \
    fonts-hack \
    libusb-0.1-4 \
    && rm -rf /var/lib/apt/lists/* /tmp/apt-packages

RUN echo "yaml https://raw.githubusercontent.com/basler/pylon-ros-camera/master/pylon_camera/rosdep/pylon_sdk.yaml" > /etc/ros/rosdep/sources.list.d/30-pylon_camera.list
RUN wget https://dnb-public-downloads-misc.s3.eu-central-1.amazonaws.com/pylon/pylon_5.2.0.13457-deb0_amd64.deb \
    && dpkg -i pylon_5.2.0.13457-deb0_amd64.deb \
    && rm pylon_5.2.0.13457-deb0_amd64.deb

COPY install-deps.sh install-deps.sh
RUN apt-get update && rosdep update \
    && (yes | ./install-deps.sh) \
    && rm -rf /var/lib/apt/lists/* /tmp/apt-packages

RUN echo 'ALL ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN echo 'Defaults env_keep += "DEBUG ROS_DISTRO"' >> /etc/sudoers

COPY env.sh /etc/profile.d/ade_env.sh
COPY entrypoint /ade_entrypoint

ENTRYPOINT ["/ade_entrypoint"]
CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
